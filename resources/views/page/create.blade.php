@extends('layouts.main')
@section('content')
    <div class="col-md-8">
    <div class="card">
        <div class="card-header card-header-primary">
        <h4 class="card-title">Ajouter un pays</h4>
        <p class="card-category">Complete your profile</p>
        </div>
        <div class="card-body">
        <form action="{{route('pays.store')}}" method="POST">
            @method("POST")
            @csrf
            <div class="row">
            <div class="col-md-5">
                <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Libelle</label>
                <input type="text" class="form-control" name="libelle">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">description</label>
                <input type="text" class="form-control" name="description">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">code indicatif</label>
                <input type="text" class="form-control" name="code_indicatif">
                </div>
            </div>
            </div>
            <div class="row">
            <div class="col-md-6">
                <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Continent</label>
                <input type="text" class="form-control" name="continent">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Population</label>
                <input type="text" class="form-control" name="population">
                </div>
            </div>
            </div>
            <div class="row">
            <div class="col-md-12">
                <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Capitale</label>
                <input type="text" class="form-control" name ="capitale">
                </div>
            </div>
            </div>
            <div class="row">
            <div class="col-md-4">
                <div class="form-group ">
                <label class="bmd-label-floating">
                    Monaie
                </label>
                <select  class="custom-select" name="monaie">
                    <option  value="XOF">XOF</option>
                    <option  value="EUR">EUR</option>
                    <option  value="DOLLAR">DOLLAR</option>
                </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group ">
                    <label class="bmd-label-floating">
                        Langue
                    </label>
                    <select  class="custom-select" name="langue">
                        <option value="Français">Français</option>
                        <option value="Anglais">Anglais</option>
                        <option value="Arabe">Arabe</option>
                        <option value="Chinois">Chinois</option>
                        <option value="Espagnol">Espagnol</option>
                        <option value="Allemand">Allemand</option>

                    </select>
                    </div>
            </div>
            <div class="col-md-4">
                <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Superficie</label>
                <input type="text" class="form-control" name="superficie">
                </div>
            </div>
            </div>
            <div class="row">
                <div class="form-group ">
                    <label class="bmd-label-floating">
                        Laique ?
                    </label>
                    <select  class="custom-select" name="est_laique">
                        <option value="Oui">Oui</option>
                        <option value="Non">Non</option>
                    </select>
                    </div>
            </div>
            <button type="submit" class="btn btn-primary pull-right">Valider l'enregistrement</button>
            <div class="clearfix"></div>
        </form>
        </div>
    </div>
    </div>
@endsection
