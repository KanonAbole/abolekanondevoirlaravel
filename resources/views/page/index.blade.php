@extends('layouts.main');
@section('content');
<div class="col-md-12">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title ">Simple Table</h4>
        <p class="card-category"> Here is a subtitle for this table</p>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead class="text-warning">
                <tr><th>ID</th>
                <th>Libelle</th>
                <th>Description</th>
                <th>Code indicatif</th>
                <th>continent </th>
                <th>population</th>
                <th>capitale</th>
                <th>monaie</th>
                <th>langue</th>
                <th>superficie</th>
                <th>est_laique</th>
              </tr></thead>
              <tbody>

                @foreach ($pays as $pay)
                <tr>
                  <td>{{$pay->id}}</td>
                  <td>{{$pay->libelle}}</td>
                  <td>{{$pay->descrition}}</td>
                  <td>{{$pay->code_indicatif}}</td>
                  <td>{{$pay->continent}}</td>
                  <td>{{$pay->population}}</td>
                  <td>{{$pay->capitale}}</td>
                  <td>{{$pay->monaie}}</td>
                  <td>{{$pay->langue}}</td>
                  <td>{{$pay->superficie}}</td>
                  <td>{{$pay->est_laique}}</td>
                </tr>
                @endforeach
              </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection

