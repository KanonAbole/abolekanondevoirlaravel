<html lang="en" class="perfect-scrollbar-on"><head>
    <meta charset="utf-8">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>
      Material Dashboard by Creative Tim
    </title>
    <meta content="width=device-width, initial-scale=1.0, shrink-to-fit=no" name="viewport">
    <!--style-->
    @include('includes.css')
    <!--endstyle-->
  <body class="">

    <div class="wrapper ">
        <!-- sidebar -->
        @include('includes.sidebar')
        <!-- endsidebar -->


      <div class="main-panel ps-container ps-theme-default ps-active-y" data-ps-id="4be9599b-dfb3-d669-e213-92f6183b4026">
        <!-- Navbar -->
        @include('includes.navbar')
        <!-- End Navbar -->
        <div class="content">
            <div class="container-fluid">
              <div class="row">
                @yield('content')
              </div>
            </div>
        </div>

        <!-- footer -->
        @include('includes.footer')
        <!-- endfooter -->

    </div>
    <!--   Core JS Files   -->
    @include('includes.js')

  </body>
