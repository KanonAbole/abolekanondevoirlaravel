


<div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
    <div class="logo"><a href="http://www.creative-tim.com" class="simple-text logo-normal">

      </a></div>
    <div class="sidebar-wrapper ps-container ps-theme-default" data-ps-id="cfc0b35e-5fb6-87bc-ef87-78536b273ad7">
      <ul class="nav">
        <li class="nav-item active  ">
          <a class="nav-link" href={{('/')}}>
            <i class="material-icons"></i>
            <p>Dashboard</p>
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="{{url('/pays')}}">
            <i class="material-icons"></i>
            <p>Listes des pays</p>
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="{{url('/enreg_pays')}}">
            <i class="material-icons"></i>
            <p>Ajouter des pays</p>
          </a>
        </li>
      </ul>
    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
  <div class="sidebar-background" style="background-image: url(../assets/img/sidebar-1.jpg) "></div></div>

