<?php

use App\Http\Controllers\PaysController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.main');
});
Route::get('/pays', [PaysController::class, 'index'])->name('pays.index');
Route::get('/enreg_pays', [PaysController::class, 'create'])->name('pays.create');

Route::post('/store', [PaysController::class, 'store'])->name('pays.store');
