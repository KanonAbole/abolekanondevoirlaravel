<?php

use Illuminate\Database\DBAL\TimestampType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pays', function (Blueprint $table) {
            $table->id();
            $table->string('libelle')->nullable();
            $table->string('description')->nullable();
            $table->string('code_indicatif')->nullable();
            $table->string('continent')->nullable();
            $table->integer('population')->nullable();
            $table->string('capitale');
            $table->string('monaie')->nullable();
            $table->string('langue')->nullable();
            $table->integer('superficie')->nullable();
            $table->string('est_laique');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pays');
    }
}
