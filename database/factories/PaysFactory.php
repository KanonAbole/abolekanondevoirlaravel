<?php

namespace Database\Factories;

use App\Models\pays;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaysFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = pays::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'libelle' => $this ->faker ->country,
            'description' => $this ->faker->sentence,
            'code_indicatif' =>$this->faker->countryCode,
            'continent' =>$this->faker->randomElement(['EUROPE','AMERIQUE','ASIE','AFRIQUE']),
            'capitale' =>$this->faker->city,
            'population'=>$this->faker->numberBetween(),
            'monaie'=>$this->faker->randomElement(['XOF','EUR','DOLLAR']),
            'langue'=>$this->faker->randomElement(['Français', 'Anglais','chinois','Noushi','Arabe']),
            'superficie'=>$this->faker->randomElement([300.000, 400.000,1700.000,224.000,338.000]),
            'est_laique'=>$this->faker->randomElement([true, false])

        ];
    }
}

